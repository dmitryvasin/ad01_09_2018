package by.itacademy.lesson4.time;

public class Main {
    public static void main(String[] args) {
        TimeInterval halfAndHour = new TimeInterval(5400);
        TimeInterval hour = TimeInterval.buildFromConsole();
        System.out.println(halfAndHour);
        System.out.println(hour);
        System.out.println(halfAndHour.compareTo(hour));
        System.out.println(hour.compareTo(halfAndHour));
    }
}
