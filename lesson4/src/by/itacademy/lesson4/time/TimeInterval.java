package by.itacademy.lesson4.time;

import java.util.Scanner;

public class TimeInterval {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeInterval(int seconds) {
        this(seconds / 3600, (seconds / 60) % 60, seconds % 60);
    }

    public TimeInterval(int hours, int minutes, int seconds) {
        this.seconds = seconds % 60;
        int totalMinutes = minutes + seconds / 60;
        this.minutes = totalMinutes % 60;
        this.hours = hours + totalMinutes / 60;
    }

    public int compareTo(TimeInterval another) {
        if (this == another) return 0;

        return this.totalSeconds() < another.totalSeconds() ? -1 :
               this.totalSeconds() > another.totalSeconds() ? 1 : 0;
    }

    public int totalSeconds() {
        return hours * 3600 + minutes * 60 + seconds;
    }

    @Override
    public String toString() {
        return "TimeInterval{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }

    public static TimeInterval buildFromConsole() {
        System.out.println("Введите интервал в секундах:");
        Scanner scanner = new Scanner(System.in);
        return new TimeInterval(scanner.nextInt());
    }
}
