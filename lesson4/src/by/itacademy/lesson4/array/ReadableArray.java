package by.itacademy.lesson4.array;

import java.util.Scanner;

public class ReadableArray {
    private int[] array;

    public ReadableArray(int length) {
        array = new int[length];
    }

    public ReadableArray(int[] array) {
        this.array =  new int[array.length];
        for (int i = 0; i < this.array.length; i++) {
            this.array[i] = array[i];
        }
    }

    public void input() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите Ваш массив:");
        for (int i = 0; i < array.length; i++) {
            array[i] = in.nextInt();
        }
    }

    public int[] array() {
        return array;
    }
}