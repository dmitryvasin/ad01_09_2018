package by.itacademy.lesson4.array;

public class SortableArray {
    private int[] array;

    public SortableArray(int[] array) {
        this.array = new int[array.length];
        for (int i = 0; i < this.array.length; i++) {
            this.array[i] = array[i];
        }
    }

    public void sort() {
        for (int i = 0; i < array.length; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    public int[] array() {
        return array;
    }
}
