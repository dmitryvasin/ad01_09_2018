package by.itacademy.lesson4.hospital;

public class Main {
    public static void main(String[] args) {
        Hospital hospital = new Hospital(3);
        hospital.add(0, new Patient(new FIO("Jack", "Jack", "Jack"), 55, true));
        hospital.add(1, new Patient(new FIO("Ben", "Ben", "Ben"), 35, false));
        hospital.add(2, new Patient(new FIO("Matt", "Matt", "Matt"), 55, true));

        hospital.searchByName();
        hospital.searchByAge();
    }
}
