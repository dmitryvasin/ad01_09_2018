package by.itacademy.lesson4.hospital;

import java.util.Scanner;

public class SearchByAgeRead {
    private Scanner scanner = new Scanner(System.in);

    public int execute() {
        System.out.println("Введите число поиска по возрасту: ");
        return scanner.nextInt();
    }
}
