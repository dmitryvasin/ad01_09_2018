package by.itacademy.lesson4.hospital;

public class Hospital {
    private PatientRead read = new PatientRead();
    private SearchByNameRead nameRead = new SearchByNameRead();
    private SearchByAgeRead ageRead = new SearchByAgeRead();

    private Patient[] patients;

    public Hospital(int size) {
        this.patients = new Patient[size];
    }

    public void input() {
        for (int i = 0; i < patients.length; i++) {
            patients[i] = read.execute();
        }
    }

    public void add(int index, Patient patient) {
        patients[index] = patient;
    }

    public void searchByName() {
        String name = nameRead.execute();
        for (int i = 0; i < patients.length; i++) {
            String namePatient = patients[i].getFio().getName();
            if (namePatient.equals(name)) {
                System.out.println(patients[i]);
            }
        }
    }

    public void searchByAge() {
        int age = ageRead.execute();
        for (int i = 0; i < patients.length; i++) {
            int agePatient = patients[i].getAge();
            if (agePatient == age) {
                System.out.println(patients[i]);
            }
        }
    }
}
