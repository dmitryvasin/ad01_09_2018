package by.itacademy.lesson4.hospital;

import java.util.Scanner;

public class PatientRead {
    private Scanner scanner = new Scanner(System.in);

    public Patient execute() {
        System.out.println("Введите данные пациента!");

        System.out.println("Введите имя:");
        String name = scanner.next();
        System.out.println("Введите фамилию:");
        String lastName = scanner.next();
        System.out.println("Введите отчество:");
        String middleName = scanner.next();
        System.out.println("Сколько лет: ");
        int age = scanner.nextInt();
        System.out.println("Болен ли:");
        boolean healthy = scanner.nextBoolean();

        return new Patient(new FIO(name, lastName, middleName), age, healthy);
    }
}