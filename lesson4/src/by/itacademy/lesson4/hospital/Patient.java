package by.itacademy.lesson4.hospital;

public class Patient {
    private FIO fio;
    private int age;
    private boolean healthy;

    public Patient(FIO fio, int age, boolean healthy) {
        this.fio = fio;
        this.age = age;
        this.healthy = healthy;
    }

    public FIO getFio() {
        return fio;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "fio=" + fio +
                ", age=" + age +
                ", healthy=" + healthy +
                '}';
    }
}
