public class ArrayFindMinMax {
    public static void main(String[] args) {
        int[] array = {20, 56, 3, 56, 1, 12, 5, 34, 89, 10};

        int max = array[0];
        int maxIndex = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                maxIndex = i;
            }
        }
        System.out.println("Наибольшее значение в массиве: " + max);

        int min = array[0];
        int minIndex = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                minIndex = i;
            }
        }
        System.out.println("Наименьшее значение в массиве: " + min);

        array[minIndex] = 0;
        array[maxIndex] = 99;

        System.out.print("[");
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.print(array[array.length - 1]);
        System.out.print("]");
    }
}