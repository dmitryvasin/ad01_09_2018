public class IsCircleCoverRectangle {
    public static void main(String[] args) {
        int length = 8;
        int width = 6;
        double radius = 4.9;
        double expectedRadius = Math.sqrt(length * length + width * width) / 2;
        if (expectedRadius <= radius)
            System.out.println("The circle cover the rectangle");
        else
            System.out.println("The circle does not cover the rectangle");
    }
}
