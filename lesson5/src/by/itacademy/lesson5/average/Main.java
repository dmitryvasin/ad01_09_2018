package by.itacademy.lesson5.average;

import java.util.Calendar;

public class Main {
    public static void main(String[] args) {
        Calendar now = Calendar.getInstance();
        Calendar birth = Calendar.getInstance();
        birth.set(2016, 5, 21);
        TimeDifference diff = new TimeDifference(birth, now);
        TimeInterval interval = new TimeInterval(diff.inYears(), diff.inMonths(), diff.inDays());
        System.out.println(interval);
    }
}
