package by.itacademy.lesson5.average;

import java.util.Calendar;

public class TimeDifference {
    private Calendar birth;
    private Calendar now;

    public TimeDifference(Calendar birth, Calendar now) {
        this.birth = birth;
        this.now = now;
    }

    public int inYears() {
        Calendar birth = (Calendar) this.birth.clone();

        if (birth.after(now)) return 0;

        return diff(birth, Calendar.YEAR);
    }

    public int inMonths() {
        Calendar birth = (Calendar) this.birth.clone();

        birth.add(Calendar.YEAR, inYears());

        if (birth.after(now)) return 0;

        return diff(birth, Calendar.MONTH);
    }

    public int inDays() {
        Calendar birth = (Calendar) this.birth.clone();

        birth.add(Calendar.YEAR, inYears());
        birth.add(Calendar.MONTH, inMonths());

        if (birth.after(now)) return 0;

        return diff(birth, Calendar.DAY_OF_MONTH);
    }

    private int diff(Calendar birth, int calendarUnit) {
        int diff = 0;
        while (birth.before(now)) {
            birth.add(calendarUnit, 1);
            diff++;
        }
        return diff - 1;
    }
}

