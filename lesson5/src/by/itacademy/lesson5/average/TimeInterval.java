package by.itacademy.lesson5.average;

public class TimeInterval {
    private int years;
    private int months;
    private int days;

    public TimeInterval(int years, int months, int days) {
        this.years = years;
        this.months = months;
        this.days = days;
    }

    @Override
    public String toString() {
        return "TimeInterval{" +
                "years=" + years +
                ", months=" + months +
                ", days=" + days +
                '}';
    }
}
