package by.itacademy.lesson5.atm;

public interface Deposit {
    boolean debit(int amount);
}
