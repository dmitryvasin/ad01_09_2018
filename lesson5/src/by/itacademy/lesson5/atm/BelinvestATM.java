package by.itacademy.lesson5.atm;

public class BelinvestATM extends AbstractATM implements Deposit {

    public BelinvestATM(int hundreds, int fifties, int twenties) {
        super(hundreds, fifties, twenties);
    }

    @Override
    public boolean debit(int amount) {
        int hundreds = amount / 100;
        int fifties = amount % 100 / 50;
        int twenties = amount % 100 % 50 / 20;

        amount -= 100 * hundreds + 50 * fifties + 20 * twenties;

        if (amount == 0) {
            this.plus(Banknote.HUNDRED, hundreds);
            this.plus(Banknote.FIFTY, fifties);
            this.plus(Banknote.TWENTY, twenties);
            return true;
        }
        return false;
    }

    @Override
    public String name() {
        return "Belinvest Bank";
    }

    @Override
    public String manufacturer() {
        return "China Inc.";
    }
}
