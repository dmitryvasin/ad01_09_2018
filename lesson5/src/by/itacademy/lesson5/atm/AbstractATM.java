package by.itacademy.lesson5.atm;

public abstract class AbstractATM implements Withdraw, Balance, Bank, Vendor {
    private int hundreds;
    private int fifties;
    private int twenties;

    protected AbstractATM(int hundreds, int fifties, int twenties) {
        this.hundreds = hundreds;
        this.fifties = fifties;
        this.twenties = twenties;
    }

    @Override
    public int show() {
        return 100 * hundreds + 50 * fifties + 20 * twenties;
    }

    @Override
    public boolean credit(int amount) {
        int hundreds = amount / 100;
        int fifties = amount % 100 / 50;
        int twenties = amount % 100 % 50 / 20;

        amount -= 100 * hundreds + 50 * fifties + 20 * twenties;

        if (amount == 0) {
            int remainingHundreds = this.hundreds - hundreds;
            int remainingFifties = this.fifties - fifties;
            int remainingTwenties = this.twenties - twenties;
            if (remainingHundreds >= 0 && remainingFifties >= 0 && remainingTwenties >= 0) {
                this.hundreds -= hundreds;
                this.fifties -= fifties;
                this.twenties -= twenties;
                return true;
            }
        }

        return false;
    }

    protected enum Banknote {
        HUNDRED, FIFTY, TWENTY
    }

    protected void plus(Banknote banknote, int count) {
        if (banknote == Banknote.HUNDRED) {
            hundreds += count;
        } else if (banknote == Banknote.FIFTY) {
            fifties += count;
        } else if (banknote == Banknote.TWENTY) {
            twenties += count;
        }
    }

    @Override
    public String toString() {
        return name() + "{" +
                "hundreds=" + hundreds +
                ", fifties=" + fifties +
                ", twenties=" + twenties +
                '}';
    }
}