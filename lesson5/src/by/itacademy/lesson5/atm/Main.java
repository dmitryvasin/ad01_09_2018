package by.itacademy.lesson5.atm;

public class Main {
    public static void main(String[] args) {
        BelinvestATM atm = new BelinvestATM(0, 0, 0);
        System.out.println(atm.show());
        atm.debit(100);
        atm.debit(100);
        atm.debit(50);
        atm.debit(50);
        atm.debit(50);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        atm.debit(20);
        System.out.println(atm.show());
        atm.credit(420);
        System.out.println(atm.show());
        System.out.println(atm.name() + ", " + atm.manufacturer());
        System.out.println(atm);
    }
}
