package by.itacademy.lesson5.atm;

public interface Vendor {
    String manufacturer();
}
