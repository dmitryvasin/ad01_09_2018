package by.itacademy.lesson11;

import java.util.Date;

public class Patient {
    private int number;
    @Generate
    private String name;
    private Date birthday;
    private boolean healthy;

    public Patient(int number, String name, Date birthday, boolean healthy) {
        this.number = number;
        this.name = name;
        this.birthday = birthday;
        this.healthy = healthy;
    }

    public Patient() {
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", healthy=" + healthy +
                '}';
    }
}