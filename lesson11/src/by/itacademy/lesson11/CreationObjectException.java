package by.itacademy.lesson11;

public class CreationObjectException extends Exception {
    public CreationObjectException(String message) {
        super(message);
    }
}