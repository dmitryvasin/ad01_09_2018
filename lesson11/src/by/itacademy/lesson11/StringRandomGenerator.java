package by.itacademy.lesson11;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StringRandomGenerator implements RandomGenerator<String> {
    private Random random = new Random();
    private List<String> names = new ArrayList<>();
    {
        names.add("Vasya");
        names.add("Petya");
        names.add("Sasha");
        names.add("Sergey");
        names.add("Dmitry");
        names.add("Alex");
    }

    @Override
    public String get() {
        return names.get(random.nextInt(names.size()));
    }
}