package by.itacademy.lesson11;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ObjectGenerator {
    private Map<Class<?>, RandomGenerator<?>> randoms = new HashMap<>();
    {
        randoms.put(String.class, new StringRandomGenerator());
    }

    public <T> T createObject(Class<T> clazz) throws CreationObjectException {
        try {
            Constructor<T> constructor = clazz.getConstructor();
            T instance = constructor.newInstance();

            Map<String, Class<?>> fieldTypes = new HashMap<>();
            for (Field field : clazz.getDeclaredFields()) {
                if (field.isAnnotationPresent(Generate.class)) {
                    fieldTypes.put(field.getName(), field.getType());
                }
            }

            for (Map.Entry<String, Class<?>> entry : fieldTypes.entrySet()) {
                String key = entry.getKey();
                String setter = "set" + Character.toUpperCase(key.charAt(0)) + key.substring(1);
                Method method = clazz.getMethod(setter, entry.getValue());
                method.invoke(instance, randoms.get(entry.getValue()).get());
            }

            return instance;
        } catch (NoSuchMethodException | IllegalAccessException
                | InstantiationException | InvocationTargetException e) {
            throw new CreationObjectException(e.getMessage());
        }
    }
}