package by.itacademy.lesson11;

public class Main {
    public static void main(String[] args) throws CreationObjectException {
        ObjectGenerator objectGenerator = new ObjectGenerator();
        System.out.println(objectGenerator.createObject(Patient.class));
    }
}