package by.itacademy.lesson11;

public interface RandomGenerator<T> {
    T get();
}